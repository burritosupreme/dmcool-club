<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');

Route::get('/contact', 'PagesController@contact');

Route::get('/about', 'PagesController@about');

Route::resource('profiles', 'ProfilesController');

/*
 *  GET /monsters (index)
 *
 *  GET /monsters/create (create)
 *
 *  GET /monsters/1 (show)
 *
 *  POST /monsters (store)
 *
 *  GET /monsters/1/edit (edit)
 *
 *  PATCH /monsters/1 (update)
 *
 *  DELETE /monsters/1 (destroy)
 */

Route::resource('monsters', 'MonstersController');
//Route::resource('monsters', 'MonstersController')->middleware('can:update,monster');


Route::post('/monsters/{monster}/actions', 'MonsterActionsController@store');

Route::patch('/actions/{action}', 'MonsterActionsController@update');

/*
Route::get('/monsters', 'MonstersController@index');

Route::get('/monsters/create', 'MonstersController@create');

Route::get('monsters/{monster}', 'MonstersController@show');

Route::post('/monsters', 'MonstersController@store');

Route::get('/monsters/{monster}/edit', 'MonstersController@edit');

Route::patch('/monsters/{monster}', 'MonstersController@update');

Route::delete('monsters/{monster}', 'MonstersController@destroy');
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

