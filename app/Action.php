<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $guarded = [];
    //
    public function monster()
    {
        return $this->belongsTo(Monster::class);
    }
}
