<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function monsters(){
        return $this->belongsToMany(Monster::class)->withTimestamps();
    }
}
