<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Monster;

class MonstersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $monsters = Monster::where('user_id', auth()->id())->get();

        return view('monsters.index', compact('monsters'));
    }

    public function create(){

        return view('monsters.create');
    }

    public function show(Monster $monster) {
        //abort_if($monster->user_id !== auth()->id(), 403);
        $this->authorize('update', $monster);
        return view('monsters.show', compact('monster'));
    }

    public function edit(Monster $monster) { //dmcool.club/monsters/{monster}/edit
        $this->authorize('update', $monster);
        return view('monsters.edit', compact('monster'));
    }

    public function update(Monster $monster) {
        $this->authorize('update', $monster);

        $monster->update(request(['title', 'description']));

        return redirect('/monsters');
    }

    public function destroy(Monster $monster){
        $this->authorize('update', $monster);
        $monster->delete();

        return redirect('/monsters');
    }



    public function store(){
        $attributes = request()->validate([
            'title' => ['required', 'min:3', 'max:128'],
            'description' => ['required', 'min:3']
        ]);

        $attributes['user_id'] = auth()->id();

        Monster::create($attributes);

        return redirect('/monsters');
    }
}
