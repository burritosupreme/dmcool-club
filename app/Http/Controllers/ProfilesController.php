<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class ProfilesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    /*
    public function index()
    {
        $profiles = Profile::where('user_id', auth()->id())->get();


        return view('profiles.index', compact('profiles'));
    }
    */

    public function show(Profile $profile)
    {
        //$this->authorize('update', $profile);



        return view('profiles.show', compact('profile'));
    }

    public function edit(Profile $profile){
        $this->authorize('update', $profile);
        return view('profiles.edit', compact('profile'));
    }

    public function update(Profile $profile){
        $this->authorize('update', $profile);

        $profile->update(request(['description', 'website', 'discord']));

        return view('profiles.show', compact('profile'));
    }

/*
    public function store(){
        $attributes = request()->validate([
            'description',
            'website',
            'discord'
        ]);

        $attributes['user_id'] = auth()->id();

        Profile::create($attributes);


        return redirect('/profiles');
    }*/
}
