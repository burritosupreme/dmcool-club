<?php

namespace App\Http\Controllers;

use App\Monster;
use Illuminate\Http\Request;

use App\Action;

class MonsterActionsController extends Controller
{
    //

    public function store(Monster $monster){
        $attributes = request()->validate(['description' => 'required']);
        $monster->addAction($attributes);

        return back();
    }

    public function update(Action $action){

        $action->update([
            'legendary' => request()->has('legendary')
        ]);

        return back();
    }
}
