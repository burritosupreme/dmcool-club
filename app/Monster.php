<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monster extends Model
{
    //
    protected $guarded = [
        'id', 'created_at'
    ];

    public function actions(){
        return $this->hasMany(Action::class);
    }

    public function addAction($action){
        $this->actions()->create($action);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }
}
