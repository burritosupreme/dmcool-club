<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    //protected $primaryKey = 'id';

    protected $guarded = [
        'id', 'created_at'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
