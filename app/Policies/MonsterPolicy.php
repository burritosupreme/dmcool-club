<?php

namespace App\Policies;

use App\User;
use App\Monster;
use Illuminate\Auth\Access\HandlesAuthorization;

class MonsterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the monster.
     *
     * @param  \App\User  $user
     * @param  \App\Monster  $monster
     * @return mixed
     */
    public function update(User $user, Monster $monster)
    {
        return $monster->user_id == $user->id;
    }
}
