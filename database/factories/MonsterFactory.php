<?php

use Faker\Generator as Faker;

$factory->define(App\Monster::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(App\User::class)->create()->id;
        },
        'title' => $faker->name,
        'description' => $faker->paragraph,
    ];
});
