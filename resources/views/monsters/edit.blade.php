@extends('layout')

@section('content')

    <h1 class="title">Edit Monster</h1>

    <form method="post" action="/monsters/{{ $monster->id }}">
        @method('PATCH')
        @csrf

        <div class="field">
            <label  class="label" for="title">Title</label>

            <div class="control">
                <input type="text" class="input" name="title" placeholder="Title" value="{{ $monster->title }}">
            </div>
        </div>

        <div class="field">
            <label class="label" for="description">Description</label>

            <div class="control">
                <textarea name="description" class="textarea" >{{ $monster->description }}</textarea>
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link">Update Monster</button>
            </div>
        </div>

    </form>

    <form method="post" action="/monsters/{{ $monster->id }}">
        @method('DELETE')
        @csrf

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link">Delete Monster</button>
            </div>
        </div>
    </form>
@endsection
