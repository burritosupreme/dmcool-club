@extends('layout')

@section('content')

<h1 class="title">Create a New Monster</h1>

<form method="post" action="../monsters">
    {{ csrf_field() }}
    <div class="field">
        <label class="label" for="title">Monster Title</label>

        <div class="control">
            <input type="text" class="input {{ $errors->has('title') ? 'is-danger' : '' }}" name="title" value="{{ old('title') }}" required>
        </div>
    </div>

    <div class="field">
        <label class="label" for="description">Monster Description</label>

        <div class="control">
            <textarea name="description" class="textarea {{ $errors->has('title') ? 'is-danger' : '' }}" value="{{ old('description') }}" required></textarea>
        </div>
    </div>
    <div class="field">
        <div class="control">
            <button type="submit" class="button is-link">Create Monster</button>
        </div>
    </div>

    @include('errors')

</form>

@endsection
