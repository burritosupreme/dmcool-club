@extends('layout')

@section('content')


        <h1 class="title is-2">Monsters</h1>
        <div class="content">
            <ul>
                @foreach($monsters as $monster)
                    <li>
                        <a href="/monsters/{{ $monster->id }}">
                            {{$monster->title}}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>

@endsection
