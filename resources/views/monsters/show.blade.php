@extends('layout')



@section('title')
DM Cool Club - Monster Viewer
@endsection

@section('head')


@endsection


@section('content')
    <h1 class="title">{{$monster->title}}</h1>


    <div class="content">
        {{$monster->description}}

        <p>
            <a href="/monsters/{{$monster->id}}/edit">Edit</a>
        </p>
        </div>

    @if ($monster->actions->count())
        <div class="box">
            @foreach($monster->actions as $action)

                <form method="POST" action="/actions/{{$action->id}}">
                    @method('PATCH')
                    @csrf
                    <label class="checkbox" for="completed">
                        <input type="checkbox" name="legendary" onchange="this.form.submit()" {{ $action->legendary ? 'checked' : '' }}>
                        {{$action->description}}
                    </label>
                </form>

            @endforeach
        </div>
    @endif


    <form method="POST" action="/monsters/{{ $monster->id }}/actions" class="box">
        @csrf
        <div class="field">
            <label  class="label" for="description">New Action</label>

            <div class="control">
                <input type="text" class="input" name="description" placeholder="New Action" required>
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link">Add Action</button>
            </div>
        </div>

        @include('errors')
    </form>


@endsection
