@extends('layout')

@section('content')

    <h1 class="title">Edit Profile</h1>

    <form method="post" action="/profiles/{{ $profile->id }}">
        @method('PATCH')
        @csrf

        <h1 class="title">{{$profile->name}}</h1>

        <div class="field">
            <label class="label" for="description">Description</label>

            <div class="control">
                <textarea name="description" class="textarea" >{{ $profile->description }}</textarea>
            </div>
        </div>

        <div class="field">
            <label class="label" for="website">Website</label>

            <div class="control">
                <textarea name="website" class="textarea" >{{ $profile->website }}</textarea>
            </div>
        </div>

        <div class="field">
            <label class="label" for="discord">Discord</label>

            <div class="control">
                <textarea name="discord" class="textarea" >{{ $profile->discord }}</textarea>
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link">Update Profile</button>
            </div>
        </div>

    </form>
@endsection
