@extends('layout')

@section('title')
    {{$profile->user->name}}'s Profile
@endsection


@section('content')



    <div class="box">
        <article class="media">
            <div class="media-content">
                <div class="content">
                    <h1 class="title">{{$profile->user->name}}</h1>
                    <p>
                        <a href="/profiles/{{$profile->id}}/edit">Edit</a>
                    </p>

                </div>
                <div class="content">
                    <p><strong>About {{$profile->user->name}}</strong>
                    <br>
                        {{$profile->description}}
                    </p>
                </div>
                <div class="media">
                <div class="content">
                    <p><strong>Website:
                        </strong>
                    {{$profile->website}}
                    </p>
                </div>
                </div>
                <div class="media">
                <div class="content">
                    <p><strong>Discord
                        </strong>
                        {{$profile->discord}}
                    </p>
                </div>
                </div>
            </div>
        </article>
    </div>


     {{--
        <h2>{{$profile->description}}</h2>
        <h2>{{$profile->description}}</h2>


        <p>
            <a href="/profiles/{{$profile->id}}/edit">Edit</a>
        </p>
    </div>--}}



@endsection
