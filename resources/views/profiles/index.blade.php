@extends('layout')

@section('content')


    <h1 class="title is-2">Profiles</h1>
    <div class="content">
        <ul>
            @foreach($profiles as $profile)
                <li>
                    <a href="/profiles/{{ $profile->id }}">
                        {{$profile->id}}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

{{--@if($profile==null)--}}
    {{--<div class="field">--}}
        {{--<div class="control">--}}
            {{--<a class="button is-link" href="../profiles/create">Create Profile</a>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endif--}}

@endsection
