<nav class="navbar is-light" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="/">
            <img src="{{asset('images/dmcc-nav-logo.png')}}" width="112" height="28">
        </a>

        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="dmccnavbar">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div id="dmccnavbar" class="navbar-menu">
        <div class="navbar-start">
            <a class="navbar-item" href="/">
                Home
            </a>

            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link" href="/monsters">
                    Monsters
                </a>

                <div class="navbar-dropdown">
                    <a class="navbar-item" href="/monsters/create">
                        Create a Monster
                    </a>
                </div>
            </div>

            <a class="navbar-item" href="/about">
                About
            </a>

            <a class="navbar-item" href="/contact">
                Contact
            </a>
        </div>

        <div class="navbar-end">
            @guest
            <div class="navbar-item">
                <div class="buttons">
                    <a class="button is-primary" href="/register">
                        <strong>Sign up</strong>
                    </a>
                    <a class="button is-light" href="/login">
                        Log in
                    </a>
                </div>
            </div>
            @else
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link" href="#">
                    {{Auth::user()->name}}
                </a>

                <div class="navbar-dropdown">
                    <a class="navbar-item" href="/profiles/{{Auth::user()->profile->id}}">
                        Profile
                    </a>
                    <hr class="navbar-divider">
                    <a class="navbar-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
            @endguest

        </div>
    </div>
</nav>
