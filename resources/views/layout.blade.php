<!doctype html>
<html>
<head>
    @yield('head')
    <title>@yield('title', 'DM Cool Club')</title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css">
</head>
<body>

    @include('navbar')

    <div class="container">
        <div class="box">
            @yield('content')
        </div>
    </div>
</body>
</html>
